using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace AllBets
{
    public class Spawner : MonoBehaviour
    {   
        #region Settings
            public int simultaneousSpawnCapacity = 1;
        #endregion
        
        #region References
            private Transform dispenserLid;
            private Vector3 deltaPosition;
        #endregion

        #region Monobehaviour Functions
            private void Awake() 
            {
                dispenserLid = transform.GetChild(0);
                deltaPosition = dispenserLid.position - transform.position;

                dispenserLid.position = transform.position;

                DOTween.Init();
            }

        #endregion

        public void Dispense(GameObject prefab, int amount)
        {
            print($"[System] {gameObject.name}: Preparing to dispense {prefab.name}: {amount}.");

            Sequence sequence = DOTween.Sequence();

            GameObject instance;
            sequence.Append(
                dispenserLid.DOBlendableMoveBy(deltaPosition, 1f)
                    .OnComplete(() =>
                    {
                        for (int i= 0; i< amount; i++)
                            if (ObjectPool.Instance.TrySpawn(prefab, out instance))
                            {
                                Vector3 playerDirection = (PlayerController.Instance.transform.position - transform.position).normalized;
                                float playerAngle = (playerDirection.y < 0? -1:1) * Mathf.Acos(playerDirection.x);
                                
                                float halfArc = Mathf.PI/8;
                                float x = Random.value * 2*halfArc - halfArc;
                                x += playerAngle;

                                Vector3 deltaPosition = new Vector3(Mathf.Cos(x),Mathf.Sin(x));
                                instance.GetComponent<ISpawnable>().Initialise(
                                    transform.position + deltaPosition,
                                    prefab); 

                                Rigidbody2D rb = instance.GetComponentInChildren<Rigidbody2D>();
                                rb.AddForce(
                                    rb.mass*(instance.transform.position - transform.position).normalized * 25f, 
                                    ForceMode2D.Impulse);
                            }
                    })
            );
            sequence.AppendInterval(.5f);
            sequence.Append(
                dispenserLid.DOBlendableMoveBy(-deltaPosition, 1f)
            );

            sequence.Play();
        }
    }
}
