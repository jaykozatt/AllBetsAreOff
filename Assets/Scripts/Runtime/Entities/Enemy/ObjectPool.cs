using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AllBets
{
    public class ObjectPool : StaticInstance<ObjectPool>
    {
        #region Structures
            [System.Serializable]
            public struct Template
            {
                public int maxCount;
                public GameObject prefab;
            }
        #endregion

        #region References
            public List<Template> templates;
            private Dictionary<string,Queue<GameObject>> pool;
            private int nonInteractingLayer;
        #endregion
        
        #region Variables & Switches
            // GracePeriod gracePeriod; // a bonus to the spawn interval that diminishes with time.
            public int populationCount {get; private set;}
            public int maxPopulation {get; private set;}
        #endregion
        
        #region Monobehaviour Functions
            protected override void Awake() 
            {
                base.Awake();
                pool = new Dictionary<string, Queue<GameObject>>();
                
                nonInteractingLayer = LayerMask.NameToLayer("Non-Interacting");
                populationCount = 0;
                maxPopulation = 0; 
                
                // Instantiate each enemy from the pool
                foreach (Template template in templates)
                {
                    GameObject enemy;
                    for(int i=0; i<template.maxCount; i++)
                    {
                        enemy = Instantiate(
                            template.prefab, 
                            transform.position, 
                            Quaternion.identity, 
                            transform
                        ).gameObject;
                        enemy.SetActive(false);

                        if (!pool.ContainsKey(template.prefab.name))
                            pool[template.prefab.name] = new Queue<GameObject>();

                        pool[template.prefab.name].Enqueue(enemy);
                    }

                    maxPopulation += template.maxCount;
                }
            }
        #endregion

        public bool TrySpawn(GameObject prefab, out GameObject instance)
        {
            if (pool[prefab.name].Count > 0)
            {
                instance = pool[prefab.name].Dequeue();
                instance.SetActive(true);
                
                populationCount++;
                print($"[System] Spawned {prefab.name} successfully.");

                return true;
            }

            instance = null;
            print($"[System] Failed to spawn {prefab.name}.");
            return false;
        }

        public void Despawn(GameObject entity)
        {
            entity.SetActive(false);
            entity.layer = nonInteractingLayer;
            entity.transform.position = transform.position;
            GameObject prefab = entity.GetComponent<ISpawnable>().SourcePrefab;

            populationCount--;
            pool[prefab.name].Enqueue(entity);

            print($"[System] Despawned {entity.name} successfully.");
        }
    }
}
