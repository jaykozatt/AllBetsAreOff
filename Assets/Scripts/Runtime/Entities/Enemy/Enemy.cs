using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Experimental.Rendering.Universal;
using DG.Tweening;
using Pathfinding;

namespace AllBets
{
    public class Enemy : MonoBehaviour, ISpawnable
    {

        #region Settings
        [Header("Settings")]
            [SerializeField] private bool isSwarmer = false;
            public float telegraphTime = 1f;
            public float impulseForce = 5; // The force applied to make it move on short bursts
            public int scorePerChip = 100; // Score value of each chip on the chipStack
            public Color lightColor = Color.yellow;
            [ColorUsage(false,true)] public Color alertColor; // A flash of color when about to attack

        #endregion

        #region References
            
            #region FMOD SFX
                [Header("SFX")]
                public FMODUnity.EventReference tackleSFX;
                protected FMOD.Studio.EventInstance tackleInstance;

                public FMODUnity.EventReference chipsSFX;
                protected FMOD.Studio.EventInstance chipsInstance;

                public FMODUnity.EventReference slideSFX;
                protected FMOD.Studio.EventInstance slideInstance;
            #endregion

        [Header("References")]
            [HideInInspector] public GameObject sourcePrefab;
            public TextPopup scorePopup;
            protected Rigidbody2D rb;
            protected Collider2D collider2d;
            protected SpriteRenderer telegraph;
            [SerializeField] protected List<SpriteRenderer> chipStack;
            protected int defaultLayer;
            protected int nonInteractingLayer;
        #endregion

        #region Variables & Switches
            protected int startingChips;
            public int numberOfChips {get; protected set;}
            protected bool isAttacking = false;
            protected string chipParticleName;
            protected Sequence attackAnimation;
            public GameObject SourcePrefab {get=>sourcePrefab;}
            public bool IsSwarmer {get=> isSwarmer;}
        #endregion

        #region Coroutine References
            protected Coroutine behaviourAI;
        #endregion

        #region Coroutine Definitions
            IEnumerator FollowAI(Transform target)
            {
                Vector2 direction;
                Vector3 targetScale = new Vector3(1.1f, .8f, 1f);
                Vector3 originalScale = Vector3.one;
                Transform sprite = transform.Find("Sprite");

                WaitForSeconds telegraphWait = new WaitForSeconds(telegraphTime);
                while (GameManager.Instance.gameState < GameState.Ended)
                {
                    if (GameManager.Instance.gameState != GameState.Playing)
                    {
                        yield return null;
                    }
                    else
                    {
                        // Sleep for a while before next attack
                        yield return new WaitForSeconds(Random.Range(2,5));
                        yield return new WaitForSeconds(Random.Range(.2f,.5f)*numberOfChips/2);

                        // Commit to an attack direction
                        direction = (target.position - transform.position).normalized;

                        sprite.DOScale(targetScale, 1f)
                            .SetEase(Ease.OutQuad)
                            .OnComplete(() =>
                                sprite.DOScale(originalScale, .2f)
                                    .SetEase(Ease.InOutElastic)
                            )
                        ;

                        // Rotate the telegraph cone to the direction
                        Quaternion rotation = telegraph.transform.rotation;
                        rotation.eulerAngles = new Vector3(
                            0,0, Mathf.Rad2Deg * 
                            (direction.y < 0? -1:1) * Mathf.Acos(direction.x)
                        );
                        telegraph.transform.rotation = rotation;

                        // Signal an incoming attack
                        SetTelegraphEnabled();
                        yield return telegraphWait;
                        SetTelegraphEnabled(false);

                        // if target is destroyed, abort and check game state
                        if (target == null || !target.gameObject.activeInHierarchy) continue;

                        // Halt movement for moment when a roulette ball starts bouncing
                        if (RouletteBall.AnyIsBouncing) 
                            yield return new WaitForSeconds(Random.Range(.1f,.3f));

                        // Execute the attack
                        rb.AddForce(rb.mass * direction * impulseForce, ForceMode2D.Impulse);
                        isAttacking = true;
                        slideInstance.start();

                        
                    }
                }
            }
        #endregion

        #region Monobehaviour Functions
            protected void OnCollisionEnter2D(Collision2D other) 
            {
                if (other.collider.CompareTag("Player") && isAttacking && !PlayerController.Instance.IsInvulnerable)
                {
                    PlayerController.Instance.GetHurt(-other.relativeVelocity);

                    // Play the Impact VFX
                    VFXSystem.Instance.PlayAt("VFX_ImpactBig", other.GetContact(0).point);
                }
                else if (other.relativeVelocity.sqrMagnitude > 25)
                {
                    VFXSystem.Instance.PlayAt("VFX_ImpactSmall", other.GetContact(0).point);
                    // rb.velocity = other.relativeVelocity / 4;
                }
            }

            protected void OnCollisionStay2D(Collision2D other) 
            {
                if (other.collider.CompareTag("Player") && isAttacking && !PlayerController.Instance.IsInvulnerable)
                {
                    PlayerController.Instance.GetHurt(-other.relativeVelocity);

                    // Play the Impact VFX
                    VFXSystem.Instance.PlayAt("VFX_ImpactBig", other.GetContact(0).point);
                }
            }
            protected virtual void OnDisable() 
            {
                if (behaviourAI != null) StopCoroutine(behaviourAI);
                Wire.Instance?.TryDetangle(gameObject);
            }

            protected virtual void OnDestroy()
            {
                if (behaviourAI != null) StopCoroutine(behaviourAI);
                
                tackleInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                chipsInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
                slideInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

                tackleInstance.release();
                chipsInstance.release();
                slideInstance.release();

                if (DiceController.Instance != null)
                    Wire.Instance.TryDetangle(gameObject);
            }

            protected virtual void Awake() 
            {
                rb = GetComponentInChildren<Rigidbody2D>();
                collider2d = GetComponent<Collider2D>();
                telegraph = GetComponentInChildren <SpriteRenderer>();
                GetComponentsInChildren<SpriteRenderer>(chipStack);
                chipStack.Remove(telegraph);
                chipStack.RemoveAt(chipStack.Count-1);
                chipParticleName = (CompareTag("Wrap Immune") ? "VFX_BlueEnemy" : "VFX_PurpleEnemy");

                startingChips = chipStack.Where(chip => chip.gameObject.activeInHierarchy).Count();
                
                telegraph.color = lightColor;
                foreach (SpriteRenderer sprite in chipStack) {
                    sprite.material.SetColor("_EmissionColor", alertColor);
                }

                defaultLayer = gameObject.layer;
                nonInteractingLayer = LayerMask.NameToLayer("Non-Interacting");
                gameObject.layer = nonInteractingLayer;
            }

            // Start is called before the first frame update
            protected virtual void Start()
            {
                // Instance the FMOD SFXs
                tackleInstance = FMODUnity.RuntimeManager.CreateInstance(tackleSFX);
                chipsInstance = FMODUnity.RuntimeManager.CreateInstance(chipsSFX);
                slideInstance = FMODUnity.RuntimeManager.CreateInstance(slideSFX);
            }

            protected virtual void Update() 
            {
                if (rb.velocity.sqrMagnitude <= 1) isAttacking = false;
                if (GameManager.Instance.gameState == GameState.Ended && behaviourAI != null) 
                    StopCoroutine(behaviourAI);
            }

            protected virtual void FixedUpdate() 
            {
                AstarPath.active.UpdateGraphs(collider2d.bounds);
            }
        #endregion

        #region Core Functions
            public virtual void Initialise(Vector3 position, GameObject prefab)
            {
                transform.position = position;
                sourcePrefab = prefab;

                ResetChips();
                SetTelegraphEnabled(false);
                Invoke("SetInteractionEnabled", .1f);

                StartAI();
            }

            public virtual void StartAI()
            {
                if (behaviourAI != null) StopCoroutine(behaviourAI);
                behaviourAI = StartCoroutine(FollowAI(PlayerController.Instance.transform));
            }

            public virtual void StopAI()
            {
                if (behaviourAI != null) StopCoroutine(behaviourAI);
                isAttacking = false;
                SetTelegraphEnabled(false);
            }

            // Enable the correct number of chips in the stack
            protected void ResetChips()
            {
                numberOfChips = startingChips;
                for (int i = 0; i < numberOfChips; i++)
                {
                    chipStack[i].gameObject.SetActive(true);
                }
            }

            protected void SetInteractionEnabled() => gameObject.layer = defaultLayer;

            protected void SetTelegraphEnabled(bool enabled=true)
            {
                telegraph.enabled = enabled;
                foreach (SpriteRenderer sprite in chipStack)
                    sprite.material.SetInt("_EmissionEnabled", enabled? 1:0);
            }

            public void GetDamaged(int chipsOfDamage)
            {
                print($"[Game] {gameObject.name}: has suffered {chipsOfDamage} chips of damage");
                
                // Instantiate a score popup
                TextPopup instance = Instantiate(scorePopup, transform.position, Quaternion.identity);
                
                // Compute leftover amount and set the origin position for the VFX, then play the effect
                int leftover = numberOfChips-chipsOfDamage;
                if (leftover < 0)
                    VFXSystem.Instance.PlayBurstAt(chipParticleName, transform.position, chipsOfDamage);
                else
                    VFXSystem.Instance.PlayBurstAt(chipParticleName, chipStack[leftover].transform.position, chipsOfDamage);

                // Clamp the leftover amount of chips 
                numberOfChips = Mathf.Max(0, leftover);
                chipStack[numberOfChips].gameObject.SetActive(false);
                
                // Add the score
                GameManager.Instance.AddScore(chipsOfDamage * scorePerChip, instance);

                // Play the toppling SFX according to the amount of chips toppled
                chipsInstance.setParameterByName("Number of chips", chipsOfDamage);
                chipsInstance.start();

                // If stack is defeated, increase score multiplier and return stack to the pool
                if (numberOfChips < 1) 
                {
                    GameManager.Instance.IncreaseCombo();
                    Despawn();
                }    
            }

            public void Despawn()
            {
                // Stop the AI
                StopAI();

                // Attempt to disentangle from wire if it was entangled.
                Wire.Instance.TryDetangle(gameObject);

                // Return this object to the pool
                ObjectPool.Instance.Despawn(gameObject);
            }
        #endregion
        
    }
}
