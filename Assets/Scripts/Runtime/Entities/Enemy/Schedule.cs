using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace AllBets
{
    [CreateAssetMenu(fileName = "New Schedule", menuName = "All Bets Are Off!/Schedule Table", order = 0)]
    public class Schedule : ScriptableObject 
    {
        #region Data Structures
            [System.Serializable]
            public class EnemyGroup
            {
                [HideInInspector] public string name;
                [Tooltip("Reference to the prefab of this group.")]
                public GameObject enemyPrefab;
                [Tooltip("Amount of enemies of this type.")]
                public int amount;
            }
            [System.Serializable]
            public class Entry
            {
                [HideInInspector] public string name;
                [Space]
                [Tooltip("Time since last spawn.")]
                public int spawnDelay;
                [Tooltip("Group of Enemies to spawn.")]
                public List<EnemyGroup> enemyGroups;
            }    
        #endregion

        #region Variables
            [Tooltip("Whether the list of entries loops back once the last item is processed.")]
            [SerializeField] bool looping;
            [Tooltip("The list of entries specifying when to spawn and how many enemies of each type to spawn.")]
            [SerializeField] List<Entry> scheduleEntries;
            public bool IsEmpty {get=>scheduleEntries.Count <= 0;}
        #endregion

        public bool TryNextEntry(out Entry nextEntry)
        {
            if (!IsEmpty)
            {
                nextEntry = scheduleEntries[0];
                scheduleEntries.RemoveAt(0);
                
                if (looping) scheduleEntries.Add(nextEntry);

                return true;
            }
            else
            {
                nextEntry = null;
                return false;
            }
        }

        private void OnValidate() {
            int timestamp = 0;
            for (int i = 0; i<scheduleEntries.Count; i++)
            {
                timestamp += scheduleEntries[i].spawnDelay;
                scheduleEntries[i].name = 
                    $"{(timestamp/60).ToString("D")}:{(timestamp%60).ToString("D2")}   ";

                foreach(EnemyGroup group in scheduleEntries[i].enemyGroups)
                {
                    scheduleEntries[i].name +=
                    $"|   {group.amount} {group.enemyPrefab.name}s   ";

                    group.name = $"{group.amount} {group.enemyPrefab.name}s";
                } 
            }
        }
    }
}
