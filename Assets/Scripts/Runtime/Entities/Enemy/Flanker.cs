using System.Collections;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using Pathfinding;

namespace AllBets
{
    public class Flanker : Enemy 
    {
        [SerializeField] float attackRange;
        [SerializeField] float moveInterval = .75f;
        [SerializeField] float nextWaypointSqrDistance = .1f;
        Vector2 attackDirection;
        SpriteRenderer bottomHalf;
        LayerMask mask;
        Slingable slingable;
        
        public FMODUnity.EventReference revUpSFX;
        protected FMOD.Studio.EventInstance revUpInstance;

        Seeker seeker;
        Path path;
        int currentWaypoint;
        public bool IsEntangled {
            get => gameObject == Wire.Instance.entangledEntity;
        }

        Tween topSpinTween;
        Tween bottomSpinTween;

        Coroutine pathUpdateRoutine;

        private IEnumerator Tracker(Transform target)
        {
            float timer = telegraphTime;
            while (timer > 0)
            {
                // Compute the expected attack direction
                attackDirection = (target.position - transform.position).normalized;

                // Rotate the telegraph region to the direction
                Quaternion rotation = telegraph.transform.rotation;
                rotation.eulerAngles = new Vector3(
                    0,0, Mathf.Rad2Deg * 
                    (attackDirection.y < 0? -1:1) * Mathf.Acos(attackDirection.x));
                telegraph.transform.rotation = rotation;

                // Tick the timer
                yield return null;
                timer -= Time.deltaTime;
            }
        }

        private IEnumerator FlankerAI(Transform target)
        {
            WaitForSeconds telegraphWait = new WaitForSeconds(telegraphTime + 1f);
            WaitForSeconds moveWait = new WaitForSeconds(moveInterval);
            while (GameManager.Instance.gameState < GameState.Ended)
            {
                if (GameManager.Instance.gameState != GameState.Playing)
                {
                    yield return null;
                }
                else
                {
                    // Wait for an instant before initiating an attack upon spawning
                    yield return new WaitForSeconds(Random.Range(1.2f,1.5f));

                    RaycastHit2D hit = CheckLineOfSight(target);
                    while (hit.collider != null)
                    {
                        if (path == null || isAttacking) 
                        {
                            yield return null;
                        }
                        else
                        {
                            if (IsEntangled || slingable.IsProjectile)
                            {
                                yield return null;
                                continue;
                            }

                            attackDirection = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                            rb.AddForce(rb.mass * attackDirection * impulseForce / 4, ForceMode2D.Impulse);

                            float sqrDistance = (path.vectorPath[currentWaypoint] - transform.position).sqrMagnitude;
                            if (sqrDistance < nextWaypointSqrDistance) currentWaypoint++;

                            yield return moveWait;
                        }

                        hit = CheckLineOfSight(target);
                    }

                    if (IsEntangled || slingable.IsProjectile)
                    {
                        yield return null;
                        continue;
                    }

                    // Start charging up the attack (animation)
                    HighRev();

                    // Start the tracking subroutine
                    StartCoroutine(Tracker(target));

                    // Signal an incoming attack
                    SetTelegraphEnabled();
                    yield return telegraphWait;

                    SetTelegraphEnabled(false);

                    // if target is destroyed, abort and check game state
                    if (target == null && !target.gameObject.activeInHierarchy) continue;

                    // Halt movement for moment when a roulette ball starts bouncing
                    if (RouletteBall.AnyIsBouncing) 
                        yield return new WaitForSeconds(Random.Range(.1f,.3f));

                    // Release the attack (animation)
                    LowRev();

                    // Abort the attack if this entity gets entangled
                    if (IsEntangled || slingable.IsProjectile)
                    {
                        yield return null;
                        continue;
                    }

                    // Execute the attack
                    rb.AddForce(rb.mass * attackDirection * impulseForce, ForceMode2D.Impulse);
                    isAttacking = true;
                    slideInstance.start();

                    yield return new WaitForSeconds(Random.Range(1,2));
                }
            }
        }

        private void HighRev()
        {
            topSpinTween?.Kill();
            topSpinTween = chipStack[0].transform.DOBlendableRotateBy(360*Vector3.forward, .25f, RotateMode.FastBeyond360)
                .SetLoops(-1)
                .SetRelative()
                .SetEase(Ease.Linear)
            ;

            bottomSpinTween?.Kill();
            bottomSpinTween = bottomHalf.transform.DOBlendableRotateBy(-360*Vector3.forward, .25f, RotateMode.FastBeyond360)
                .SetLoops(-1)
                .SetRelative()
                .SetEase(Ease.Linear)
            ;

            transform.Find("Sprite").DOScale(new Vector3(.7f, .7f, 1f), telegraphTime+1f)
                .SetEase(Ease.OutQuad);

            revUpInstance.start(); // Play the Rev Up SFX
        }

        private void LowRev()
        {
            topSpinTween?.Kill();
            topSpinTween = chipStack[0].transform.DOBlendableRotateBy(360*Vector3.forward, 2f, RotateMode.FastBeyond360)
                .SetLoops(-1)
                .SetRelative()
                .SetEase(Ease.Linear)
            ;

            bottomSpinTween?.Kill();
            bottomSpinTween = bottomHalf.transform.DOBlendableRotateBy(-360*Vector3.forward, 2f, RotateMode.FastBeyond360)
                .SetLoops(-1)
                .SetRelative()
                .SetEase(Ease.Linear)
            ;

            transform.Find("Sprite").DOScale(Vector3.one, .2f)
                .SetEase(Ease.InOutElastic);

            revUpInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        }

        private RaycastHit2D CheckLineOfSight(Transform target)
        {
            RaycastHit2D hit = Physics2D.Raycast(
                transform.position,
                (target.position - transform.position).normalized,
                attackRange,
                mask
            );

            return hit;
        }

        public override void Initialise(Vector3 position, GameObject prefab)
        {
            base.Initialise(position, prefab);

            LowRev();
            slingable.Initialise();

            if (pathUpdateRoutine != null) StopCoroutine(pathUpdateRoutine);
            pathUpdateRoutine = StartCoroutine(UpdatePath(PlayerController.Instance.transform));
        }

        public override void StartAI()
        {
            if (behaviourAI != null) StopCoroutine(behaviourAI);
            behaviourAI = StartCoroutine(FlankerAI(PlayerController.Instance.transform));
        }

        public override void StopAI()
        {
            base.StopAI();
            LowRev();
        }
        protected override void Awake()
        {
            base.Awake();
            chipStack.RemoveAt(chipStack.Count-1);
            chipStack.RemoveAt(chipStack.Count-1);

            seeker = GetComponent<Seeker>();
            bottomHalf = transform.Find("Sprite/BottomChip").GetComponent<SpriteRenderer>();
            slingable = GetComponent<Slingable>();
            startingChips = chipStack.Where(chip => chip.gameObject.activeInHierarchy).Count();
            mask = LayerMask.GetMask("Default", "Non-Actor");
            chipParticleName = "VFX_Flanker";

        }

        protected override void Start()
        {
            base.Start();
            revUpInstance = FMODUnity.RuntimeManager.CreateInstance(revUpSFX);
        }

        protected override void OnDisable() {
            base.OnDisable();
            if (pathUpdateRoutine != null) StopCoroutine(pathUpdateRoutine);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (pathUpdateRoutine != null) StopCoroutine(pathUpdateRoutine);
        }

        protected IEnumerator UpdatePath(Transform target)
        {
            WaitForSeconds wait = new WaitForSeconds(.5f);
            while (true)
            {
                if (seeker.IsDone() && target != null)
                    seeker.StartPath(transform.position, target.position, OnPathComplete);
                else if (target == null)
                    break;
                
                yield return wait;
            }

        }

        protected void OnPathComplete(Path p) 
        {
            if (!p.error)
            {
                path = p;
                currentWaypoint = 0;
            }
        }
    }
}