using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace AllBets
{
    public class EntityScheduler : StaticInstance<EntityScheduler> 
    {
        #region Data Structures
            [System.Serializable]
            public struct SpawnerArray 
            {
                public Spawner center;
                public List<Spawner> outerRim;
            }
        #endregion

        #region Schedule Originals
            [Header("Schedules")]
            [SerializeField] Schedule mainSchedule;   
            [SerializeField] Schedule loopingSchedule;
        #endregion

        #region Variables & Switches
            Schedule _mainSchedule;
            Schedule _loopingSchedule;
            Schedule.Entry _currentEntry;
            int _remainingAmount = 0;
        #endregion

        #region References
            [Header("References")]
            public SpawnerArray spawners;
        #endregion

        #region Coroutine References
            Coroutine _distributeRoutine;
        #endregion

        private void Start() 
        {
            // BeginSchedule();
            _mainSchedule = Instantiate(mainSchedule);
            _loopingSchedule = Instantiate(loopingSchedule);
        }

        public void BeginSchedule()
        {
            _distributeRoutine = StartCoroutine(Distribute(_mainSchedule));
        }

        private IEnumerator Distribute(Schedule schedule)
        {
            while (schedule.TryNextEntry(out _currentEntry) && GameManager.Instance.gameState < GameState.Ended)
            {
                // Await until the timestamp is reached
                yield return new WaitForSeconds(_currentEntry.spawnDelay);

                // Order spawners by proximity to player character
                Queue<Spawner> outerSpawners = new Queue<Spawner>(
                    spawners.outerRim.OrderBy(s => 
                        (s.transform.position - PlayerController.Instance.transform.position).sqrMagnitude
                    )
                );

                // Spawn as much of the first enemy group at the center as possible
                int i = 0;
                _remainingAmount = _currentEntry.enemyGroups[i].amount;
                int enemiesToSpawn = Mathf.Min(_remainingAmount, spawners.center.simultaneousSpawnCapacity);
                spawners.center.Dispense(
                    _currentEntry.enemyGroups[i].enemyPrefab, 
                    enemiesToSpawn);
                _remainingAmount -= enemiesToSpawn;
                
                // print($"[System] Distributing remaining enemies in group {i}.");

                Spawner spawner;
                while (_remainingAmount > 0) 
                {
                    spawner = outerSpawners.Dequeue();

                    enemiesToSpawn = Mathf.Min(_remainingAmount, spawner.simultaneousSpawnCapacity);
                    spawner.Dispense(
                        _currentEntry.enemyGroups[i].enemyPrefab, 
                        enemiesToSpawn);
                    _remainingAmount -= enemiesToSpawn;

                    outerSpawners.Enqueue(spawner);
                }

                // Then, distribute the rest of the enemy group among the other spawners, 
                // with closest proximity to player given priority.
                while (i+1 < _currentEntry.enemyGroups.Count)
                {
                    // print($"[System] Distributing remaining enemies in group {i+1}.");

                    _remainingAmount = _currentEntry.enemyGroups[++i].amount;
                    while (_remainingAmount > 0) 
                    {
                        spawner = outerSpawners.Dequeue();

                        // Find out if this enemy type is meant to be spawned in swarms or individually
                        bool entityIsSwarmer = _currentEntry.enemyGroups[i].enemyPrefab.GetComponent<ISpawnable>().IsSwarmer;                        

                        enemiesToSpawn = entityIsSwarmer ? Mathf.Min(_remainingAmount, spawner.simultaneousSpawnCapacity) : 1;
                        spawner.Dispense(
                            _currentEntry.enemyGroups[i].enemyPrefab, 
                            enemiesToSpawn);
                        _remainingAmount -= enemiesToSpawn;

                        outerSpawners.Enqueue(spawner);
                    }

                }
            }
            
            // Begin distributing entities with the looping schedule
            if (schedule != _loopingSchedule) 
                _distributeRoutine = StartCoroutine(Distribute(_loopingSchedule));
        }
    }
}