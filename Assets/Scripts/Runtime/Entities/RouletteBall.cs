using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace AllBets
{
    public class RouletteBall : MonoBehaviour, ISpawnable
    {
        #region Settings
        [Header("Settings")]
            public int bouncesUntilDestroy = 10;
            public float speedThreshold = 15;
            public string movingLayerName = "Moving";
            [SerializeField] private float _explosionRadius = 5f;
            [SerializeField] private bool isSwarmer = false;
        #endregion

        #region FMOD SFX
        [Header("SFX")]
            [SerializeField] FMODUnity.EventReference impactSFX;
            [SerializeField] FMODUnity.EventReference chimeSFX;
            [SerializeField] FMODUnity.EventReference explosionSFX;
        #endregion

        #region References
            public Rigidbody2D rb {get; private set;}
            public static List<RouletteBall> ballList;
            private GameObject sourcePrefab;
        #endregion

        #region Variables & Switches
            private int _bounceCounter = 0;
            private float speedSqrThreshold;
            private int defaultLayer;
            private int movingLayer;
            private LayerMask mask;
            public bool IsSwarmer {get => isSwarmer;}
            public GameObject SourcePrefab {get=>sourcePrefab;}
        #endregion

        #region Properties
            public static bool AnyIsBouncing {
                get =>  
                    ballList?.Any(ball => ball?.rb?.velocity.sqrMagnitude > ball?.speedSqrThreshold) ??
                    false
                ;
            }
        #endregion

        #region Monobehaviour Functions
            #region Events
                private void OnCollisionEnter2D(Collision2D other) 
                {
                    int tacklingLayer = LayerMask.NameToLayer("Player Tackling");
                    if (other.gameObject.layer == tacklingLayer || other.gameObject.layer == movingLayer)
                        this.SetLethal();

                    if (rb.velocity.sqrMagnitude < speedSqrThreshold)
                        VFXSystem.Instance.PlayAt("VFX_ImpactSmall", other.GetContact(0).point);
                    else
                    {
                        VFXSystem.Instance.PlayAt("VFX_ImpactMedium", other.GetContact(0).point);
                        if (other.gameObject != DiceController.Instance.gameObject)
                            _bounceCounter++;
                    }
                    
                    Enemy enemy;
                    if (other.gameObject.TryGetComponent<Enemy>(out enemy) && 
                        gameObject.layer == movingLayer &&
                        rb.velocity.sqrMagnitude > speedSqrThreshold)
                    {
                        enemy.GetDamaged(enemy.numberOfChips);
                        FMODUnity.RuntimeManager.PlayOneShot(chimeSFX, transform.position);
                    }
                    else
                    {
                        FMODUnity.RuntimeManager.PlayOneShot(impactSFX, transform.position);
                    }

                    if (_bounceCounter >= bouncesUntilDestroy) 
                    {
                        Collider2D[] affectedList =
                            Physics2D.OverlapCircleAll(transform.position, _explosionRadius, mask);

                        ISpawnable spawnable;
                        foreach (Collider2D affected in affectedList)
                        {
                            if (affected.TryGetComponent<ISpawnable>(out spawnable))
                            {
                                if (spawnable is Enemy e) e.GetDamaged(e.numberOfChips);
                                else spawnable.Despawn();
                            }
                        }
                    }
                }

                private void OnDestroy() 
                {
                    ballList?.Remove(this);
                }
            #endregion

            private void Awake() 
            {
                if (ballList == null) ballList = new List<RouletteBall>();
                ballList.Add(this);

                defaultLayer = gameObject.layer;
                movingLayer = LayerMask.NameToLayer(movingLayerName);
                mask = LayerMask.GetMask("Default", "SuperDefault", "Moving Ball", "Non-Actor");

                speedSqrThreshold = speedThreshold * speedThreshold;
            }

            private void Start() 
            {
                rb = GetComponent<Rigidbody2D>();
            }

            private void Update() 
            {
                if (rb.velocity.sqrMagnitude < speedSqrThreshold)
                    this.SetLethal(false);
                else if (Wire.Instance.entangledEntity == this.gameObject && rb.velocity.sqrMagnitude >= speedSqrThreshold)
                    this.SetLethal();
            }

            public void Initialise(Vector3 position, GameObject prefab)
            {
                transform.position = position;
                sourcePrefab = prefab;
                SetLethal(false);

                _bounceCounter = 0;
            }

            public void SetLethal(bool value=true) 
            {
                gameObject.layer = value ? movingLayer : defaultLayer;
            }

            public void Despawn()
            {
                VFXSystem.Instance.PlayAt("VFX_SmallExplosion", transform.position);
                FMODUnity.RuntimeManager.PlayOneShot(explosionSFX, transform.position);

                Wire.Instance.TryDetangle(gameObject);

                CameraController.Instance.Shake(2);
                ObjectPool.Instance.Despawn(gameObject);
            }
        #endregion
    }
}
