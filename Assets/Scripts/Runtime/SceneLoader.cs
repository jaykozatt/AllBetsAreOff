using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using System.Linq;

namespace AllBets
{
    public enum Scene {
        None, MainMenu, Game
    }

    public class SceneLoader : StaticInstance<SceneLoader>
    {
        public Canvas webGLOpeningSlide;

        public CanvasGroup loadingGraphics;
        public RadialFade transitionFade;

        Scene currentScene;
        Coroutine loadSceneRoutine;
        Tween fadeTween;

        public Action<float> OnLoadingProgressUpdate; 

        protected override void Awake() 
        {
            base.Awake();
        }

        private void Start() 
        {
            StartCoroutine(StartupSequence());
        }

        public bool TryLoadScene(Scene scene)
        {
            if (loadSceneRoutine == null)
            {
                loadSceneRoutine = StartCoroutine(TransitionTo(scene));
                return true;
            } 

            print($"[System] Failed to load \"{scene}\"");
            return false;
        }

        private IEnumerator StartupSequence()
        {
            FMODUnity.RuntimeManager.LoadBank(Scene.MainMenu.ToString(), true);
            FMODUnity.RuntimeManager.LoadBank(Scene.Game.ToString(), true);

            #if UNITY_WEBGL
                // Wait for interaction
                while (!Input.anyKeyDown) yield return null;

                // Reset audio after the first interaction to make sure audio plays on WebGL builds
                FMODUnity.RuntimeManager.CoreSystem.mixerSuspend();
                FMODUnity.RuntimeManager.CoreSystem.mixerResume();
            #endif

            webGLOpeningSlide.gameObject.SetActive(false);
            
            #if UNITY_EDITOR
                if (SceneManager.sceneCount < 2)
                    TryLoadScene(Scene.MainMenu);
                else
                    loadingGraphics.gameObject.SetActive(false);
            #else
                TryLoadScene(Scene.MainMenu);
            #endif

            yield break;
        }

        private IEnumerator TransitionTo(Scene scene)
        {
            // First, fade to black
            transitionFade.CloseCurtain();

            // Wait while the fade effect is running
            while (transitionFade.isPlaying) yield return null;

            // Fade in the loading bar
            FadeInLoadingBar();

            // Unload the currently loaded scene if there is one
            AsyncOperation tracker;
            if (currentScene != Scene.None)
            {
                tracker = SceneManager.UnloadSceneAsync((int) currentScene);
                // FMODUnity.RuntimeManager.UnloadBank(currentScene.ToString());

                while (!tracker.isDone)
                {
                    OnLoadingProgressUpdate?.Invoke(tracker.progress/4f);
                    yield return null; 
                } 
            }

            // Start loading the scene
            currentScene = scene;
            tracker = SceneManager.LoadSceneAsync((int) scene, LoadSceneMode.Additive);

            // Halt scene activation until further notice
            tracker.allowSceneActivation = false;

            // Load the Audio Bank required for this scene
            // FMODUnity.RuntimeManager.LoadBank(currentScene.ToString(), true);

            // Make sure all banks are loaded before proceeding
            while (!FMODUnity.RuntimeManager.HasBankLoaded(currentScene.ToString())) yield return null;
            OnLoadingProgressUpdate?.Invoke(2/4f);

            // Make sure sample data is finished loading
            while (FMODUnity.RuntimeManager.AnySampleDataLoading()) yield return null;
            OnLoadingProgressUpdate?.Invoke(3/4f);

            // Finally, proceed with the last bit of scene loading
            tracker.allowSceneActivation = true;
            while (!tracker.isDone) 
            {
                // Post the current load progress to all listeners
                OnLoadingProgressUpdate?.Invoke(3/4f + tracker.progress/4f);
                yield return null;
            }

            // Send the completion message to all listeners
            OnLoadingProgressUpdate?.Invoke(1);
            print($"[System] Loaded \"{scene}\" successfully.");

            // Fade out the loading bar
            FadeOutLoadingBar();

            // Lastly, reveal the scene
            transitionFade.OpenCurtain();

            loadSceneRoutine = null; 
        }

        private IEnumerator FadeInLoadingBar()
        {
            OnLoadingProgressUpdate?.Invoke(0);
            loadingGraphics.gameObject.SetActive(true);
            Tween tween = loadingGraphics.DOFade(1,.25f);
            while (tween.IsActive()) yield return null;
        }
        private void FadeOutLoadingBar()
        {
            loadingGraphics.DOFade(0,.25f)
                .OnComplete(()=>loadingGraphics.gameObject.SetActive(false))
            ;
        }
    }
}
