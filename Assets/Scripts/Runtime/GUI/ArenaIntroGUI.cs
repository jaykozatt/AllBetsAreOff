using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Threading.Tasks;

namespace AllBets
{
    public class ArenaIntroGUI : MonoBehaviour
    {
        public float fadeDuration = .5f; 
        CanvasGroup group;
        public TextMeshProUGUI pressAnyKeyText;

        // Start is called before the first frame update
        void Awake()
        {
            group = GetComponentInChildren<CanvasGroup>();
            #if UNITY_ANDROID || UNITY_IOS 
                pressAnyKeyText.text = "Tap anywhere to begin...";
            #endif
        }

        public async void Submit_Start()
        {
            group.interactable = false;
            group.DOFade(0, fadeDuration);
            await Task.Delay((int)(fadeDuration * 1000));
            GameManager.Instance.EnableGUI();
            GameManager.Instance.BeginGame();
        }
    }
}
