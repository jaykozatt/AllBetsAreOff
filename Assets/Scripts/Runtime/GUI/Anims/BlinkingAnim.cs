using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace AllBets
{
    public class BlinkingAnim : MonoBehaviour
    {
        TextMeshProUGUI _textMesh;
        [SerializeField] float _interval = 1f;

        private void OnDisable() {
            StopAllCoroutines();
        }

        void Awake()
        {
            _textMesh = GetComponent<TextMeshProUGUI>();
        }

        private void OnEnable() {
            StartCoroutine(AnimationRoutine());
        }

        IEnumerator AnimationRoutine()
        {
            var delay = new WaitForSeconds(_interval);
            while (true)
            {
                yield return delay;
                _textMesh.enabled = false;

                yield return delay;
                _textMesh.enabled = true;
            }
        }
    }
}
