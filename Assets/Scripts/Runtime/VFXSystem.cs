using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AllBets
{
    public class VFXSystem : StaticInstance<VFXSystem>
    {
        Dictionary<string,ParticleSystem> vfx;

        protected override void Awake()
        {
            base.Awake();

            vfx = new Dictionary<string, ParticleSystem>();
            for (int i = 0; i < transform.childCount; i++)
            {
                ParticleSystem particle = transform.GetChild(i).GetComponent<ParticleSystem>();
                vfx[particle.gameObject.name] = particle;
            }

        }

        public void PlayAt(string vfxName, Vector3 position)
        {
            // ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams();
            // emitParams.position = position;

            // Set the origin position for the VFX, then play the effect
            vfx[vfxName].transform.position = position;
            // vfx[vfxName].Emit(emitParams, 1);
            vfx[vfxName].Play();
        }

        public void PlayBurstAt(string vfxName, Vector3 position, int burstCount=1)
        {
            // Get the VFX particle system's burst 
            ParticleSystem.EmissionModule emission = vfx[vfxName].emission;
            ParticleSystem.Burst burst = emission.GetBurst(0);

            ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams();
            emitParams.position = position;

            // Set how many particles are gonna be emitted
            burst.count = burstCount;
            emission.SetBurst(0, burst);
            
            // Set the origin position for the VFX, then play the effect
            vfx[vfxName].transform.position = position;
            vfx[vfxName].Emit(emitParams, burstCount);
            vfx[vfxName].Play();
        }
    }
}
