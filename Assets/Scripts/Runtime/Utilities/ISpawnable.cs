using UnityEngine;

namespace AllBets
{
    public interface ISpawnable 
    {
        GameObject SourcePrefab {get;}
        bool IsSwarmer {get;}
        void Initialise(Vector3 position, GameObject prefab);
        void Despawn();
    }
}