using UnityEngine;

namespace AllBets
{
    public class Slingable : MonoBehaviour 
    {
        #region Settings
            [Header("Settings")]
            [SerializeField] private float _lethalSpeedThreshold = 10f;
            [SerializeField] private float _explosionRadius = 5f;
            [SerializeField] private float _helperForce = 25f;
        #endregion

        #region References
            [SerializeField] FMODUnity.EventReference explosionSFX;
            Rigidbody2D rb;
            LayerMask mask;
            Vector2 direction;
            float originalDrag;
        #endregion

        public bool IsProjectile {get; private set;}
        bool HasLethalVelocity {
            get => rb.velocity.sqrMagnitude >= _lethalSpeedThreshold *_lethalSpeedThreshold;
        }    

        private void OnCollisionEnter2D(Collision2D other) 
        {
            if (IsProjectile)
            {
                // Explode
                Collider2D[] affectedList =
                    Physics2D.OverlapCircleAll(other.GetContact(0).point, _explosionRadius, mask);

                ISpawnable spawnable;
                foreach (Collider2D affected in affectedList)
                {
                    if (affected.TryGetComponent<ISpawnable>(out spawnable))
                    {
                        if (spawnable is Enemy enemy) enemy.GetDamaged(enemy.numberOfChips);
                        else spawnable.Despawn();
                    }
                }

                CameraController.Instance.ReleaseSlingedEntity();

                VFXSystem.Instance.PlayAt("VFX_SmallExplosion", other.GetContact(0).point);
                FMODUnity.RuntimeManager.PlayOneShot(explosionSFX, transform.position);
                CameraController.Instance.Shake(2);
            }
        }

        private void Awake() 
        {
            rb = GetComponent<Rigidbody2D>();
            mask = LayerMask.GetMask("Default", "SuperDefault", "Moving Ball", "Non-Actor");
            originalDrag = rb.drag;
            IsProjectile = false;
        }

        private void Update() 
        {
            if (Wire.Instance.entangledEntity == gameObject)
                rb.drag = 0.25f;
            else if (Wire.Instance.entangledEntity != gameObject && !IsProjectile)
                rb.drag = originalDrag;

            if (IsProjectile && !HasLethalVelocity)
                EnableProjectileState(false);

            // if (IsProjectile && Wire.Instance.entangledEntity == gameObject)
            // {
            //     direction = rb.velocity.normalized;
            //     rb.AddForce(direction * _helperForce * rb.mass * Time.deltaTime);
            // }
        }

        public void Initialise()
        {
            EnableProjectileState(false);
        }

        public void OnRelease()
        {
            if (HasLethalVelocity)
            {
                direction = rb.velocity.normalized;
                rb.AddForce(direction * _helperForce / 5 * rb.mass * Time.deltaTime, ForceMode2D.Impulse);

                // RouletteBall ball;
                // if (gameObject.TryGetComponent<RouletteBall>(out ball)) ball.SetLethal();
            }
        }

        public void EnableProjectileState(bool value=true) 
        {
            IsProjectile = value;
            if (!value) CameraController.Instance.ReleaseSlingedEntity();
        }
    }

}